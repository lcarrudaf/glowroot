**Configuração**

# No diretório desejado 
Ex.: /user/APM

>git clone git@gitlab.com:lcarrudaf/glowroot.git

# Em seguida entre no diretório:

>cd /user/APM/glowroot

# Para iniciar o container digite a seguinte linha de comando:

>docker-compose up

# Agente Config

https://github.com/glowroot/glowroot/tree/v0.13.6
> -javaagent:path/to/glowroot.jar


#File 

glowroot.properties
agent.id=SERVICO::SERVER_NAME:PORT
collector.address=http://172.16.0.23:8181
